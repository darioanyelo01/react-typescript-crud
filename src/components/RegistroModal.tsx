import React, { useState } from "react";
import "./RegistroModal.css";

interface Props {
  showModal: boolean;
  closeModal: () => void;
  registerUser: (username: string, email: string, password: string) => void;
}

export const RegistroModal: React.FC<Props> = ({
  showModal,
  closeModal,
  registerUser,
}) => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    registerUser(username, email, password);
  };

  return (
    <div className={showModal ? "modal display-block" : "modal display-none"}>
      <section className="modal-main">
        <form onSubmit={handleSubmit}>
          <h2>Registrarse</h2>
          <label>
            Nombre de usuario:
            <input
              type="text"
              value={username}
              onChange={(event) => setUsername(event.target.value)}
              required
            />
          </label>
          <label>
            Email:
            <input
              type="email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              required
            />
          </label>
          <label>
            Contraseña:
            <input
              type="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
              required
            />
          </label>
          <button type="submit">Registrarse</button>
          <button type="button" onClick={closeModal}>
            Cancelar
          </button>
        </form>
      </section>
    </div>
  );
};

