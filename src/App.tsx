// import React, { useState } from "react";

// import "./App.css";
// import { RegistroModal } from "./components/Modal";

// interface User {
//   id: number;
//   username: string;
//   email: string;
//   password: string;
// }

// const App: React.FC = () => {
//   const [showRegistroModal, setShowRegistroModal] = useState(false);
//   const [users, setUsers] = useState<User[]>([]);

//   const handleRegistroSubmit = (
//     username: string,
//     email: string,
//     password: string
//   ) => {
//     const newUser: User = {
//       id: users.length + 1,
//       username,
//       email,
//       password,
//     };
//     setUsers([...users, newUser]);
//     setShowRegistroModal(false);
//   };

//   return (
//     <div className="App">
//       <header>
//         <h1>Aplicación de registro de usuarios</h1>
//         <button onClick={() => setShowRegistroModal(true)}>Registrarse</button>
//       </header>
//       <main>
//         <ul>
//           {users.map((user) => (
//             <li key={user.id}>
//               <h3>{user.username}</h3>
//               <p>Email: {user.email}</p>
//             </li>
//           ))}
//         </ul>
//       </main>
//       <RegistroModal
//         showModal={showRegistroModal}
//         closeModal={() => setShowRegistroModal(false)}
//         registerUser={handleRegistroSubmit}
//       />
//     </div>
//   );
// };

// export default App;
//--------------------------------------------------------------------------------------------

// import React, { useState } from "react";
// import { Table } from "reactstrap";

// import "./App.css";
// // import { Table } from "reactstrap";
// import { RegistroModal } from "./components/Modal";

// interface User {
//   id: number;
//   username: string;
//   email: string;
//   password: string;
// }

// export const App: React.FC = () => {
//   const [showRegistroModal, setShowRegistroModal] = useState(false);
//   const [users, setUsers] = useState<User[]>([]);

//   const handleRegistroSubmit = (
//     username: string,
//     email: string,
//     password: string
//   ) => {
//     const newUser: User = {
//       id: users.length + 1,
//       username,
//       email,
//       password,
//     };
//     setUsers([...users, newUser]);
//     setShowRegistroModal(false);
//   };

//   const handleEliminarUsuario = (id: number) => {
//     const filteredUsers = users.filter((user) => user.id !== id);
//     setUsers(filteredUsers);
//   };

//   return (
//     <div className="App">
//       <header>
//         <h1>Aplicación de registro de usuarios</h1>
//         <button onClick={() => setShowRegistroModal(true)}>Registrarse</button>
//       </header>
//       <main>
//         <Table>
//           <thead>
//             <tr>
//               <th>ID</th>
//               <th>Username</th>
//               <th>Email</th>
//               <th>Acciones</th>
//             </tr>
//           </thead>
//           <tbody>
//             {users.map((user) => (
//               <tr key={user.id}>
//                 <td>{user.id}</td>
//                 <td>{user.username}</td>
//                 <td>{user.email}</td>
//                 <td>
//                   <button onClick={() => handleEliminarUsuario(user.id)}>
//                     Eliminar
//                   </button>
//                 </td>
//               </tr>
//             ))}
//           </tbody>
//         </Table>
//       </main>
//       <RegistroModal
//         showModal={showRegistroModal}
//         closeModal={() => setShowRegistroModal(false)}
//         registerUser={handleRegistroSubmit}
//       />
//     </div>
//   );
// };

//------------------------------------------------------------------------

import React, { useEffect, useState } from "react";

import "./App.css";
import { Table } from "reactstrap";
import { RegistroModal } from "./components/RegistroModal";

interface User {
  id: number;
  username: string;
  email: string;
  password: string;
}

interface Props {
  perPage?: number;
}

export const App: React.FC<Props> = ({ perPage = 5 }) => {
  const [showRegistroModal, setShowRegistroModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [editUserId, setEditUserId] = useState(0);
  const [users, setUsers] = useState<User[]>([]);
  const [buscarTermino, setBuscarTermino] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const [usuariosfiltrados, setUsuariosfiltrados] = useState<User[]>([]);

  const handleRegistroSubmit = (
    username: string,
    email: string,
    password: string
  ) => {
    const newUser: User = {
      id: users.length + 1,
      username,
      email,
      password,
    };
    setUsers([...users, newUser]);
    setShowRegistroModal(false);
  };

  const handleEditarUsuario = (id: number) => {
    setShowEditModal(true);
    setEditUserId(id);
  };

  const handleActualizarUsuario = (
    id: number,
    username: string,
    email: string,
    password: string
  ) => {
    const updatedUsers = users.map((user) =>
      user.id === id ? { id, username, email, password } : user
    );
    setUsers(updatedUsers);
    setShowEditModal(false);
  };

  const handleEliminarUsuario = (id: number) => {
    const filteredUsers = users.filter((user) => user.id !== id);
    setUsers(filteredUsers);
  };

  const handleBuscarTerminoChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setBuscarTermino(e.target.value);
  };

  useEffect(() => {
    filtrarUsuario(buscarTermino);
  }, [buscarTermino]);

  // const usuariosFiltrados = users
  // .filter(user =>
  //   user.username.toLowerCase().includes(buscarTermino.toLowerCase())
  // )
  // .slice(currentPage * perPage, (currentPage + 1) * perPage);
  const pageCambio = (listaUsuarios:User[]=[], resetear:boolean=false) => {
    let desde = currentPage * perPage;
    let hasta = (currentPage + 1) * perPage;
    if(resetear){
     desde = 0
     hasta = 5
    }
  
    return listaUsuarios.slice(desde,hasta);
  }

  const filtrarUsuario = (termino: string) => {
    if (buscarTermino === "") {
      const pages = pageCambio(users);
      setUsuariosfiltrados(pages);
    } else {
      const listaUsuarios = users.filter((user) => {
        return user.username.toLowerCase().includes(termino.toLowerCase());
      });
   
      const pages = pageCambio(listaUsuarios,true);
      setUsuariosfiltrados(pages);
    }
  };

  const handlePageClick = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  // const filteredData = users.slice(currentPage * perPage, (currentPage + 1) * perPage);

  useEffect(() => {
    if(users.length > 0){
      const pages = pageCambio(users);
      setUsuariosfiltrados(pages);
    }
  
  }, [users,currentPage])
  
 
  return (
    <div className="App">
      <header>
        <h1>Aplicación de registro de usuarios CRUD con typescript</h1>
        <button onClick={() => setShowRegistroModal(true)}>
          Botón Modal Registrarse
        </button>
      </header>
      <main>
        <Table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Username</th>
              <th>Email</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {usuariosfiltrados.map((user) => (
              <>
                <tr key={user.id}>
                  <td>{user.id}</td>
                  <td>{user.username}</td>
                  <td>{user.email}</td>
                  <td>
                    <button
                      className="editar"
                      onClick={() => handleEditarUsuario(user.id)}
                    >
                      Editar
                    </button>
                    <button
                      className="eliminar"
                      onClick={() => handleEliminarUsuario(user.id)}
                    >
                      Eliminar
                    </button>
                  </td>
                </tr>
              </>
            ))}
          </tbody>
        </Table>
        <hr />
        <div className="pagination">
          <label>Páginación</label>
          {Array.from({ length: Math.ceil(users.length / perPage) }, (_, i) => (
            <button
              key={i}
              className={`pagination-btn ${i === currentPage ? "active" : ""}`}
              onClick={() => handlePageClick(i)}
            >
              {i + 1}
            </button>
          ))}
        </div>
        <hr />
        <div className="field">
          <div className="control">
            <label>Buscador de Usuario:</label>
            <input
              className="input"
              type="text"
              placeholder="Buscar usuario..."
              value={buscarTermino}
              onChange={handleBuscarTerminoChange}
            />
          </div>
        </div>
      </main>
      <RegistroModal
        showModal={showRegistroModal}
        closeModal={() => setShowRegistroModal(false)}
        registerUser={handleRegistroSubmit}
      />
      <RegistroModal
        showModal={showEditModal}
        closeModal={() => setShowEditModal(false)}
        registerUser={(username, email, password) =>
          handleActualizarUsuario(editUserId, username, email, password)
        }
      />
    </div>
  );
};
